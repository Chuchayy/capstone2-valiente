let navItems = document.querySelector("#navSession");

let userGreet = document.querySelector("#userGreet")

//localStorage => an object used to store information locally in our device
let userToken = localStorage.getItem("token");

/*localStorage {
	isAdmin: false,
	getItem: function()
}*/


//conditional rendering
//dynamically add or delete an html element based on a condition
//here we are adding our login and register links in the navbar  ONLY if the user is not logged in
//Otherwise, only the logout link will be shown.
//innerHTML = contains all of the element's children as a string
if(!userToken) {

	console.log(navItems.innerHTML)

	navItems.innerHTML += 
	`
		<li class="nav-item ">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

	`

} else{
	navItems.innerHTML += 
	`
		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Logout </a>
		</li>

	`
}

let isAdmin = localStorage.getItem('isAdmin')
if(!userToken) {

	userGreet.innerHTML +=
	`
		Hello, Guest!

	`

} else{
	if (isAdmin) {
		userGreet.innerHTML += `Hello, Admin!`
	} else{
		userGreet.innerHTML += `Hello, User!`
	}
	
}

