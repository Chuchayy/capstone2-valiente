let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

let token = localStorage.getItem("token")

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let numberOfStudent = document.querySelector("#numberOfStudent")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let hr = document.querySelector("#hr")
let listOfStudent = document.querySelector("#listOfStudent")

if(!token){

	fetch(`https://morning-brushlands-50621.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		let enrollees = data.enrollees
		let studentEnrolled = enrollees.length

		courseName.innerHTML = `${data.name}`
		courseDesc.innerHTML = `${data.description}`
		numberOfStudent.innerHTML = `Student Enrolled: ${studentEnrolled}`
		coursePrice.innerHTML = `${data.price}`

		enrollContainer.innerHTML = 
		`
			<hr>
			<p>Do you want this course? Click Login or Register</p>

			<div class="container px-0">	
				<div class="row">		
					<div class="col-md-6">
						<a href="./../pages/login.html" class="nav-link btn-primary text-center">Login</a>
					</div>
					<div class="col-md-6">
						<a href="./../pages/register.html" class="nav-link btn-info text-center">Register</a>
					</div>
				</div>

			</div>
			
		`



		})

} else if(!token || isAdmin === "false"){

	fetch(`https://morning-brushlands-50621.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {

		let enrollees = data.enrollees
		let studentEnrolled = enrollees.length

		courseName.innerHTML = `${data.name}`
		courseDesc.innerHTML = `${data.description}`
		numberOfStudent.innerHTML = `Student Enrolled: ${studentEnrolled}`
		coursePrice.innerHTML = `${data.price}`

		enrollContainer.innerHTML = 
		`
			<hr>
			<p>Do you want this Course? Click Enroll to enroll course</p>
			<button id="enrollButton" class="btn btn-button btn-primary w-100">Enroll Course</button>
			<a class="btn btn-info w-100 mt-1" href="./../pages/courses.html">Back to Courses</a>
		`

		document.querySelector("#enrollButton").addEventListener("click", () => {

			//no need to add e.preventDefault because the clikc event does not have a default behavior that refreshes the page unlike submit

			fetch('https://morning-brushlands-50621.herokuapp.com/api/users/enroll',{

				method: 'POST',
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data){
					alert("You have enrolled successfully")
					window.location.replace("./courses.html")
				} else {
					alert("Enrollment Failed")
				}
			})
		})
	})

} else {

	fetch(`https://morning-brushlands-50621.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {	
		let enrollees = data.enrollees
		let studentEnrolled = enrollees.length
		console.log(studentEnrolled)
		let userId = enrollees.userId
		

		courseName.innerHTML = `${data.name}`
		courseDesc.innerHTML = `${data.description}`
		numberOfStudent.innerHTML = `Student Enrolled: ${studentEnrolled}`
		coursePrice.innerHTML = `${data.price}`

		hr.innerHTML = `<hr>`
		listOfStudent.innerHTML = `<p>List of Students</p>`

		adminBackButton.innerHTML = 
		`
			<a class="btn btn-info w-100" href="./../pages/courses.html">Back to Courses</a>
		`

		let courseData
		
		
		
		
		if(enrollees < 1){
			courseData = "No enrollees found"
		} else {
			
			courseData = enrollees.map(course => {
				console.log(course)

				let userId = course.userId

				let userData

				let users = []
				localStorage.setItem('userArray', JSON.stringify(users))

				fetch(`https://morning-brushlands-50621.herokuapp.com/api/users/${userId}`,{
					headers: {
						"Authorization": `Bearer ${token}`
					}
				})
				.then(res => res.json())
				.then(user => {
					console.log(user)

					users = JSON.parse(localStorage.getItem('userArray'))

					users.push(user)

					localStorage.setItem('userArray', JSON.stringify(users))

					users = JSON.parse(localStorage.getItem('userArray'))

					console.log(users.length)

					if(users < 1){
						userData = "No Enrollees Found"
					} else {
						userData = users.map(users => {
							console.log(users)

							return(
									`
										<div class="col-md-12 my-3 bg-white">
											<div = "card">
												<div class="card-body">
													<h5 class="cart-title text-center">${users.firstName} ${users.lastName}</h5>
													<p class="card-text text-center">Enrolled On: ${course.enrolledOn}</p>
												</div>
											</div>
										</div>
									`
								)
						}).join("")

					}
					enrollContainer.innerHTML = userData


				})

			})

		}

		let usersArray = JSON.parse(localStorage.getItem('userArray'))
		console.log(usersArray)

		/*let array = JSON.parse(localStorage.getItem(userArray))
		console.log(array)*/

	})

}
