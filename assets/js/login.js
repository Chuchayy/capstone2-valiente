/*get the login form and store in a variable*/
let loginForm = document.querySelector("#logInUser")

/*add a submit event to our login form, so that when the button is click to submit, we are able to run a function.*/

loginForm.addEventListener("submit", (e) => {

	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	/*console.log(email)
	console.log(password)
*/
	//check if our user was able to submit an email or password, if not, we'll show an alert, if yes, we'll proceed to the login fetch request.

	if(email == "" || password == ""){

		alert("Please input your email/password")

	} else{


		fetch('https://morning-brushlands-50621.herokuapp.com/api/users/login', {

			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			
			//store our token in our localStorage.
			//localStorage is a storage for data in most modern browsers.
			//localStorage, however, can onyl store strings.

			//check if data/response is false or null, if there is no token or false is returned we will not save it in the localStorage.

			if(data !== false && data.accessToken !== null){

				//if we are able to get a token, we'll then use it to get our user's details.
				//store the token in the localStorage

				localStorage.setItem('token', data.accessToken)

				fetch('https://morning-brushlands-50621.herokuapp.com/api/users/details', {

					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {
					
					localStorage.setItem('id', data._id)
					localStorage.setItem('isAdmin', data.isAdmin)
					localStorage.setItem('courses', JSON.stringify(data.enrollments))

					console.log(localStorage)

					window.location.replace('./courses.html')


				})

			} else {

				alert('Login Failed. Something went wrong.')

			}


		})
	}
})