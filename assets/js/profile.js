let profileContainer = document.querySelector("#profileContainer")

let params = new URLSearchParams(window.location.search)

let token = localStorage.getItem("token")

let userName = document.querySelector("#userName")
let userMobNum = document.querySelector("#userMobNum")
let backButton = document.querySelector("#backButton")

let enrolledCourseContainer = document.querySelector("#enrolledCourseContainer")

fetch(`https://morning-brushlands-50621.herokuapp.com/api/users/${userId}`,{
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data)

	userName.innerHTML = `${data.firstName} ${data.lastName}`
	userMobNum.innerHTML += `Mobile Number: ${data.mobileNo}`

	backButton.innerHTML =`<a href="./../pages/courses.html" class="nav-link btn-info"> Back to Courses </a>`

	let enrollments = data.enrollments
	console.log(enrollments)

	let userData


	userData = enrollments.map(user => {
		console.log(user)

		let courseId = user.courseId

		let courses = []

		localStorage.setItem('courseArray', JSON.stringify(courses))

		let courseData

		fetch(`https://morning-brushlands-50621.herokuapp.com/api/courses/${courseId}`)
		.then(res => res.json())
		.then(course => {

			courses = JSON.parse(localStorage.getItem('courseArray'))

			courses.push(course)

			localStorage.setItem('courseArray', JSON.stringify(courses))

			courses = JSON.parse(localStorage.getItem('courseArray'))
			console.log(courses)

			if(course < 1){
				courseData = "No Courses Found"
			} else {

				courseData = courses.map(courses => {

					return (

						`
							<div class="col-md-12 my-3 bg-white">
								<div = "card">
									<div class="card-body">
										<h5 class="cart-title text-center">${courses.name}</h5>
										<p class="card-text text-center">Enrolled On: ${user.enrolledOn}</p>
										<p class="card-text text-center">Status: ${user.status}</p>
									</div>
								</div>
							</div>
						`

					)
				}).join("")
			}enrolledCourseContainer.innerHTML = courseData
		})
	})
})